﻿using UnityEngine;

public abstract class GoalSock : MonoBehaviour
{
    public GameObject otherSock;

    [HideInInspector]
    public FollowTheLeader followTheLeaderScript;

    private void Start()
    {
        followTheLeaderScript = otherSock.GetComponent<FollowTheLeader>();
    }

    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && followTheLeaderScript.collected)
        {
            followTheLeaderScript.leader = this.gameObject;
        }
    }
}
