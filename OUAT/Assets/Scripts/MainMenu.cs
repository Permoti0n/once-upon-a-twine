﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public void OnButtonSwitchScene()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public void OnButtonExitGame()
    {
        Application.Quit();
    }
}
