﻿using UnityEngine;

public class BlueGoalSock : GoalSock
{
    private PlayerMovement playerMove;

    public override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
        if (other.CompareTag("Player") && followTheLeaderScript.collected)
        {
            playerMove = other.GetComponent<PlayerMovement>();
            playerMove.jumpForce = playerMove.bigJumpForce;
        }
    }
}
