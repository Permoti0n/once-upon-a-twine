﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueFollowSock : FollowTheLeader
{
    private PlayerMovement playerMove;

    public override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);

        if (other.CompareTag("Player") && !collected)
        {
            playerMove = other.GetComponent<PlayerMovement>();
            collected = true;
            playerMove.canJump = true;
            playerMove.jumpForce = playerMove.smallJumpForce;
        }
    }
}
