﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flame : MonoBehaviour
{
    public GameObject flameSprite;
    public Collider2D flameCollider;
    public float startTimer = 5f;
    public float timer = 5f;
    private bool flame;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            // Die
            Debug.Log("Du bist gedeaded");
        }
    }

    public void Update()
    {
        timer -= Time.deltaTime;

        if (timer <= 0)
        {
            FlameOnOff();
            timer = startTimer;
        }
    }

    public void FlameOnOff()
    {
        if (flame == false)
        {
            flame = true;
            flameSprite.GetComponent<SpriteRenderer>().enabled = true;
            flameCollider.enabled = true;
        }
        else
        {
            flame = false;
            flameSprite.GetComponent<SpriteRenderer>().enabled = false;
            flameCollider.enabled = false;
        }
    }
}
