﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 1.2f;
    public float maxSpeed = 4f;
    public float jumpForce = 4f;
    public float smallAndBigJumpDifference = 4f;
    public float jumpTime = 0.2f;
    public float wallJumpForce = 10f;
    public float gravityScale = 2f;
    public Transform feetPos;
    public float checkRadius = 0.6f;
    public Transform leftSidePos;
    public Transform rightSidePos;
    public float checkRadius = 0.01f;
    public LayerMask whatIsGround;

    public bool canJump = true;
    public bool canWallJump = true;

    [HideInInspector]
    public float smallJumpForce;
    [HideInInspector]
    public float bigJumpForce;

    private bool grounded;
    private bool touchLeftSide;
    private bool touchRightSide;
    //private bool lastMoveLeft = false;

    private Rigidbody2D rb;
    private float jumpTimeCounter;
    private float moveStartCounter;
    private float xInput;
    private float jumpSlowDown = 1.2f;
    private Vector2 currentVelocity;
    // Commands
    private bool jumpCommand;
    //private SpriteRenderer spriteRenderer;


    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        smallJumpForce = jumpForce - (smallAndBigJumpDifference / 2);
        bigJumpForce = jumpForce + (smallAndBigJumpDifference / 2);
        //spriteRenderer = GetComponent<SpriteRenderer>();;
    }

    private void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(transform.position, checkRadius, whatIsGround);
        currentVelocity = rb.velocity;

        // get collisions
        grounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround);

        touchLeftSide = Physics2D.OverlapCircle(leftSidePos.position, checkRadius, whatIsGround);
        touchRightSide = Physics2D.OverlapCircle(rightSidePos.position, checkRadius, whatIsGround);

        // Horizontal Movement
        currentVelocity += new Vector2(xInput * moveSpeed / jumpSlowDown, 0f);

        // Jump Movement
        if (touchLeftSide && canJump && jumpCommand && canWallJump) //left Walljump
        {
            jumpTimeCounter = jumpTime;
            currentVelocity.x += wallJumpForce;
            currentVelocity.y += jumpForce;
        }
        else if (touchRightSide && canJump && jumpCommand && canWallJump) //right Walljump
        {
            jumpTimeCounter = jumpTime;
            currentVelocity.x -= wallJumpForce;
            currentVelocity.y += jumpForce;
        }
        else if (grounded && canJump && jumpCommand) //normal jump
        {
            jumpTimeCounter = jumpTime;
            currentVelocity.y += jumpForce;
        }

        if (jumpCommand && !IsOnWall(touchLeftSide, touchRightSide)) //extra velocity for controlling jump height
        {
            if (jumpTimeCounter > 0)
            {
                rb.gravityScale = 0.1f;
                jumpTimeCounter -= Time.deltaTime;
            }
        }

        if (!jumpCommand || jumpTimeCounter <= 0)
        {
            rb.gravityScale = gravityScale;
        }


        currentVelocity.y = Mathf.Clamp(currentVelocity.y, -maxSpeed, maxSpeed * 2);
        if (!IsOnWall(touchLeftSide, touchRightSide) || grounded)
        {
            currentVelocity.x = Mathf.Clamp(currentVelocity.x, -maxSpeed, maxSpeed);
        }
        else
        {
            currentVelocity.x = Mathf.Clamp(currentVelocity.x, -maxSpeed * 2, maxSpeed * 2);
        }

        rb.velocity = currentVelocity;

        // reset Commands
        jumpCommand = false;
    }

    private bool IsOnWall(bool onLeftSide, bool onRightSide)
    {
        if (onLeftSide || onRightSide)
        {
            return true;
        }
        return false;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            jumpCommand = true;
        }

        xInput = Input.GetAxisRaw("Horizontal");

        if (xInput != 0)
        {
            moveStartCounter += Time.deltaTime;
        }
        else
        {
            moveStartCounter = 0;
        }

        //var lookingLeft = rb.velocity.x < -0.01;
        //if (Mathf.Abs(xInput) >= 0.01)
        //{
        //    spriteRenderer.flipX = lookingLeft;
        //    lastMoveLeft = lookingLeft;
        //}
    }
}
