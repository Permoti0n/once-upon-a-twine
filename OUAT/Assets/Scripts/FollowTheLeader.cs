﻿using System.Collections.Generic;
using UnityEngine;

public abstract class FollowTheLeader : MonoBehaviour
{
    public GameObject leader; // the game object to follow - assign in inspector
    public int steps; // number of steps to stay behind - assign in inspector

    private Queue<Vector2> record = new Queue<Vector2>();

    [HideInInspector]
    public bool collected = false;

    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && collected == false)
        {
            leader = other.gameObject;
        }
    }


    void FixedUpdate()
    {
        if (collected)
        {
            // record position of leader
            record.Enqueue(leader.transform.position);

            // remove last position from the record and use it for our own
            if (record.Count > steps)
            {
                this.transform.position = record.Dequeue();
            }
        }
    }
}